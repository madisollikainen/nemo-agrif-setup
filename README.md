# NEMO-v3.6 AGRIF nested models setup #

This repository holds helper scripts and templates for setting up nested global ocean models using NEMO-LIM3 and AGRIF. Additionally, it holds a short guide for new users on how to set up such models; the guide can be found in `doc` directory. The helper scripts, templates and the guide have been compiled as part of my work for prof. Raudsepp at TTÜ Marine Systems Department. 

The helper scripts can be found in `scripts` directory. They provide a help massage, when called with `-h` command line option. Many of the scripts source the `msi-hpc.setup` file, which holds compiler and path information specific to the TTÜ Marine Systems Department HPC machines. If users wish to use the scripts on a different machine, or use different compilers and/or directory structure on the MSI machine, then they should create a new set-up file based on `msi-hpc.setup`. For all scripts, which use the set-up files, users can provide their own set-up files with the `-s [setup file]` command line option. 

Templates of computer architecture configuration files for NEMO and XIOS can  be found in `arch-info` directory. In `nested-config` directory, template namelists for creating input files for nested models can be found. These namelists are used for the Weights and Nesting tools helpers shipped with NEMO. These helpers are not provided by this repository. 

The `.tex` and `.bib` files, as well as, figures for the NEMO-AGRIF guide can be found in the `doc` directory. For more information regarding downloading and compiling NEMO and its supporting libraries, setting up input files for NEMO and NEMO-AGRIF simulations and how to use the helper scripts in `scripts`, please refere to the guide `doc/MadisOllikainen-report-NEMO-AGRIF.pdf`.

