#!/bin/bash
#
# Author: 	
#		Madis Ollikainen 
#
# Functionality: 	
#		A helper script for collecting NEMO simulation output.
#
# Notes: 
#		The script expect the exitance of the 'output_file_list_orca2_lim3_xios-1.0.list'
#		file, which lists the types of files for which to search.
#

## Help Message
SCRIPT=`basename ${BASH_SOURCE[0]}`
function HELP {
	echo -e ""
	echo -e " A helper script for collecting NEMO  output files into a single directory."\\n
  echo -e " Usage: ./${SCRIPT} -o [dir] (optional: -i [dir])"\\n
  echo -e " Options: "\\n 
  echo -e "  -h\\t\\t: Creates this help message"\\n
  echo -e "  -o [dir]\\t: (Required) Sets the directory, where to collect the files."
  echo -e "          \\t  If the directory does not yet exist, it is created."
  echo -e "  -i [dir]\\t: (Optional) Sets the directory, where to search for the NEMO" 
  echo -e "          \\t  output files. Default: './'"\\n
  echo -e " Notes:"\\n
  echo -e " Currently searches for the standard NEMO output files, including nested"
  echo -e " model files. In addition, searches for 1 and 10 time step, 8 hours and "
  echo -e " 5 days data output files (written by XIOS). If your simulation produces"
  echo -e " output files with different frequencies, then just modify the FILE_TYPES"
  echo -e " variable. When some of the files, which the script is looking for, are "
  echo -e " not found, it will still work, but some small error messages are printed." 
  echo -e ""\\n
  exit 1
} 

## Variables to be recieved from commandline options
DIR=""
IN="./"

## Parse commandline args with bash built-in getopts 
while getopts ":ho:i:" opt; do
  case $opt in
		h)
      HELP
      ;;
    o)
			DIR="$OPTARG"
			;;
		i)
			IN="$OPTARG"
			;;
		\?)
      echo -e "Invalid option: -$OPTARG" >&2
      exit 1
      ;;
    :)
      echo -e "Option -$OPTARG requires an argument." >&2
      exit 1
      ;;
	esac
done

## Check if the directory was specified
if [ ! "$DIR" ]; then
  echo -e "ERROR: Need to specify the output directory!"\\n
  echo -e " Usage: ./${SCRIPT} -o [dir] (optional: -i [dir])"\\n
  exit 1
fi

## Make the directory
mkdir -p $DIR

## List of files
declare -a FILE_TYPES=(
log.log
*_restart_*.nc
*_5d_*.nc
*_1ts_*.nc
*_10ts_*.nc
*_8h_*.nc
*mesh_mask_*.nc
*layout.dat
*ocean.output
*output.namelist.*
*output.*.nc
*solver.stat
*time.step
*STRAIT.dat
*EMPave.dat)

## Move the files
for i in "${FILE_TYPES[@]}"
do
	mv $IN/$i $DIR/
done

