#!/bin/bash
#
# Author: 	
#		Madis Ollikainen 
#
# Functionality: 	
#		A helper script for generating the input fields for nested region in 
#		NEMO-AGRIF nested ocean models. 
#
# Notes:
#		The fields files for the nested region are generated from the field
#		files for the parent region; this is doen using the 'nesting tools'
#		and 'weights tools' tools, which are shipped with NEMO ocean model. 
#		The current script just automates a part of the processes. The user 
#		has to define the correct namelist files and then just run this script;
#		the script automates the part where the user has to call 6 different 
#		executables from the above mentioned tools. All of the hard work is 
#		still doen by the tools mentioned above. 
#
# Further information:
#		http://forge.ipsl.jussieu.fr/nemo/wiki/Users/SetupNewConfiguration/AGRIF-nesting-tool
#		

## Help Message
SCRIPT=`basename ${BASH_SOURCE[0]}`
function HELP {
	echo -e ""
	echo -e " Automates calling the Nesting tools and Weights tools for nested region preparation."\\n  
  echo -e " Usage: ./${SCRIPT} -i [input dir] (optional: -s [setup file])"\\n
  echo -e " Options: "\\n 
  echo -e "  -h\\t\\t: Creates this help message"\\n
  echo -e "  -i [dir]\\t: (Required) Sets [dir] as the directory where to look for the namelists\\n\\t\\t\\t     and where to create the output nested model input files."\\n
  echo -e "  -s [file]\\t: (Optional) Sets [file] as the set-up file. Default 'msi-hpc.setup'"
  echo -e ""
  echo -e " Before usage, the user has to compile the Nesting and Weights tools and provide four namelist files:"\\n
  echo -e "  *.nesting\\t\\t\\t : for Nesting tools"
  echo -e "  *-core2-bilinear.weights\\t : for Weights tools (CORE-II bilinear weights)"
  echo -e "  *-core2-bicubic.weights\\t : for Weights tools (CORE-II bicubic weights)"
  echo -e "  *-chlorophyll-bilinear.weights : for Weights tools (Chlorophyll bilinear weights)"
  # echo -e "  -n [level]\\t: (Optional) Sets [level] as the nesting level. Default is 1."
  echo -e ""\\n
  exit 1
}

## Variables to be recieved from commandline options
DIR=""												# Directory where to look for the namelists
														
SETUP_FILE="msi-hpc.setup"  	# Can manually change the setup file. It defaults 
															# to the setup file used at MSI HPC.

## Parse commandline args with bash built-in getopts
while getopts ":hs:i:" opt; do
  case $opt in
    h)
      HELP
      ;;
    s)
			SETUP_FILE = "$OPTARG"
			;;
    i)
			DIR="$OPTARG"
			;;
    \?)
      echo -e "Invalid option: -$OPTARG" >&2
      exit 1
      ;;
    :)
      echo -e "Option -$OPTARG requires an argument." >&2
      exit 1
      ;;
  esac
done


## Source the set-up file
if [ -f $SETUP_FILE ]; then
	echo -e ""
	echo -e "Using ${SETUP_FILE} as the setup file ... "
	source $SETUP_FILE
else 
	echo "ERROR: Set-up file ${SETUP_FILE} not found!"
	exit 1 
fi

## Go to working directory
if [ -d "$DIR" ]; then
  echo -e ""
	echo "Using ${DIR} as working directory ... "
	cd $DIR
else
	echo "ERROR: Working directory ${DIR} does not exist!"
	exit 1
fi

## Check if nesting namelist exists; the checking procedure is based on : 
## https://stackoverflow.com/questions/3856747/check-whether-a-certain-file-type-extension-exists-in-directory/3856879#3856879
count=`ls -1 *.nesting 2>/dev/null | wc -l`
if [[ ${count} -eq 1 ]]
then
	NESTING_NAMELIST=`ls -1 *.nesting`
	LVL="$(cut -d'_' -f1 <<<"$NESTING_NAMELIST")"
	echo "Using namelist file ${DIR}/${NESTING_NAMELIST} to create level $LVL nested region ... "
	echo ""
elif [[ ${count} -gt 1 ]]
then
	echo "ERROR : There are ${tmp-count} nesting namelist files in ${DIR}! Instead you should have only."
	echo ""
	exit
else 
	echo "ERROR : No nesting namelist file (file with .nesting extention) found in ${DIR}"
	echo ""
	exit
fi

## Make the coordinates file 
echo "Making ${LVL}_coordinates.nc file ... "
echo ""
${NESTING_TOOLS}/agrif_create_coordinates.exe ${NESTING_NAMELIST}

if [[ ! -e "${LVL}_coordinates.nc" ]]
then
	echo "ERROR : ${LVL}_coordinates.nc file not created"
	exit
fi

## Make the bathymetry file
echo "Making ${LVL}_bathy_meter.nc file ... "
echo ""
${NESTING_TOOLS}/agrif_create_bathy.exe ${NESTING_NAMELIST}	

if [[ ! -e "${LVL}_bathy_meter.nc" ]]
then
	echo "ERROR : ${LVL}_bathy_meter.nc file not created"
	exit
fi

## Make the data files 
echo "Making the data files ... "
echo ""
${NESTING_TOOLS}/agrif_create_data.exe ${NESTING_NAMELIST}

## Create a tmp directory for the weights files
TMP_DIR=tmp_weights
mkdir -p $TMP_DIR
cd $TMP_DIR

## For convinience copy the coordinates file 
cp ../${LVL}_coordinates.nc my_coordinates.nc

## Create bilinear forcing weights
if [ -f ../*-core2-bilinear.weights ]; then
	cp ../*-core2-bilinear.weights . 
	WEIGHTS_NAMELIST=`ls -1 *-core2-bilinear.weights`
	echo "${WEIGHTS_NAMELIST}" | $WEIGHTS_TOOLS/scripgrid.exe
	echo "${WEIGHTS_NAMELIST}" | $WEIGHTS_TOOLS/scrip.exe
	echo "${WEIGHTS_NAMELIST}" | $WEIGHTS_TOOLS/scripshape.exe
	mv ${LVL}_weights*.nc ../
else 
	echo "ERROR: Weights namelist file *-core2-bilinear.weights not found!"
	cd $WORK_DIR
	exit 1 
fi

## Create bicubic forcing weights
if [ -f ../*-core2-bicubic.weights ]; then
	cp ../*-core2-bicubic.weights . 
	WEIGHTS_NAMELIST=`ls -1 *-core2-bicubic.weights`
	echo "${WEIGHTS_NAMELIST}" | $WEIGHTS_TOOLS/scripgrid.exe
	echo "${WEIGHTS_NAMELIST}" | $WEIGHTS_TOOLS/scrip.exe
	echo "${WEIGHTS_NAMELIST}" | $WEIGHTS_TOOLS/scripshape.exe
	mv ${LVL}_weights*.nc ../
else 
	echo "ERROR: Weights namelist file *-core2-bicubic.weights not found!"
	cd $WORK_DIR
	exit 1 
fi

## Create bilinear chlorophyll weights
if [ -f ../*-chlorophyll-bilinear.weights ]; then
	cp ../*-chlorophyll-bilinear.weights .
	WEIGHTS_NAMELIST=`ls -1 *-chlorophyll-bilinear.weights`
	echo "${WEIGHTS_NAMELIST}" | $WEIGHTS_TOOLS/scripgrid.exe
	echo "${WEIGHTS_NAMELIST}" | $WEIGHTS_TOOLS/scrip.exe
	echo "${WEIGHTS_NAMELIST}" | $WEIGHTS_TOOLS/scripshape.exe
	mv ${LVL}_weights*.nc ../
else 
	echo "ERROR: Weights namelist file *-chlorophyll-bilinear.weights not found!"
	cd $WORK_DIR
	exit 1 
fi

## Clean up the tmp directory
cd ../
rm -r ${TMP_DIR}

## Come back to the working dir 
cd $WORK_DIR

