#!/bin/bash
#
# Author: 	
#		Madis Ollikainen 
#
# Functionality: 	
#		A helper script for setting up namelists for a nested region fields generation.
#		NEMO-AGRIF nested ocean models. 
#
# Notes:
#		This script just automates the easy and repetitive stuff based on the template   
#   namelists in the nesting-config/template directory. If the user wishes to change 
#   parameters relaited with the input file creation methods, then the created namelists
#   should be modified before use. 
#
#   !!! Always check the *_namelist_cfg files, before running NEMO simulations; the current
#   !!! script will make some dedious and easilly automated tasks, but it doesn't set 
#   !!! parameters for the physics. If the user wishes to use other physical settings than 
#   !!! the default (see the namelist files in nesting-config/template), they have to 
#   !!! make the modifications themselves.  

## Help Message
SCRIPT=`basename ${BASH_SOURCE[0]}`
function HELP {
	echo -e ""
	echo -e " Creates basic namelist files from templates for Nesting tools and Weights tools usage."
  echo -e " These basic namelist files should be checked and, if need be, modified."\\n
  echo -e " Usage: ./${SCRIPT} -d [output dir] [other options]"\\n
  echo -e " Options: "\\n 
  echo -e "  -h\\t\\t: Creates this help message"\\n
  echo -e "  -d [dir]\\t: (Required) Sets [dir] as the directory where to make the namelists."\\n
  echo -e "  -s [file]\\t: (Optional) Sets [file] as the set-up file. Default 'msi-hpc.setup'"
  echo -e "  -i [dir]\\t: (Optional) Sets [dir] as the directory where to look for the template namelists.\\n\\t\\t\\t     Default: NESTED_TEMPLATE_DIR in setup file."
  echo -e "  -p [dir]\\t: (Optional) Sets [dir] as the directory where to look for parent grid files\\n\\t\\t\\t     Default: ORCA2_DATA in setup file."
  echo -e "  -f [dir]\\t: (Optional) Sets [dir] as the directory where to look for forcing field files\\n\\t\\t\\t     Default:ORCA2_DATA in setup file."
  echo -e "  -n [level]\\t: (Optional) Sets [level] as the nesting level. Default: 1."
  echo -e "  -r [name]\\t: (Optional) Sets [name] as the name of the nested region. Default: 'Agulhas'."
  echo -e ""\\n
  echo -e " Method: "\\n
  echo -e " The script takes the template namelists in nested-config/template directory and applies"
  echo -e " the search-and-replace helper script replace-helper.sh on these the following namelist files:"\\n
  echo -e " \\tn_namelist_cfg"
  echo -e " \\ttemplate-chlorophyll-bilinear.weights"
  echo -e " \\ttemplate-core2-bicubic.weights"
  echo -e " \\ttemplate-core2-bilinear.weights"
  echo -e " \\ttemplate.nesting"\\n 
  echo -e " These template files are copied to the directory provided with -d input key. The names of the "
  echo -e " the namelists are changed to reflect the name  of the nested region and the nesting level; e.g"
  echo -e " n_namelist_cfg and template.nesting are changed to 1_namelist_cfg and 1_Agulhas.nesting. The "
  echo -e " search-and-replace helper script sets the following variables:"\\n
  echo -e " \\tnested region nesting level\\t(keyword in templates: ?NESTING-LEVEL?)"
  echo -e " \\tparent region nesting level\\t(keyword in templates: ?PARENT-LEVEL?)"
  echo -e " \\tnested region name \\t\\t(keyword in templates: ?REGION-NAME?)"
  echo -e " \\tpath to parent region files\\t(keyword in templates: ?PATH-TO-PARENT-DIR?)"
  echo -e " \\tpath to forcing files\\t\\t(keyword in templates: ?PATH-TO-FORCING-DIR?)"
  echo -e " \\tpath to topography database\\t(keyword in templates: ?PATH-TO-TOPOGRAPHY-DATA?)"\\n
  echo -e " The resultant namelist files can be used with gen-nesting-input.sh script for automating calls to "
  echo -e " the Nesting and Weights tools. The namelists for Weights tools (*.weights) can be used without extra"
  echo -e " modifications, except if the user wishes to apply different parameters to the Weights tools. The"
  echo -e " *.nesting and *_namelist_cfg namelists need to be modified to reflect the nested region position "
  echo -e " and refinement factors. Of course if the user wishes to use other setting for nesting tools," 
  echo -e " the *.nesting namelist should be modified. The user should check the *_namelist_cfg file and modify "
  echo -e " necessary fields."
  exit 1
}

## Variables to be recieved from commandline options
TEMPLATE_DIR=""             # Directory where to look for the template namelists
                            # defaults to the NESTING_TEMPLATE_DIR from the setup file

REGION_NAME="Agulhas"       # Name of the region; defaults to Agulhas

LVL=1                       # Nesting level; defaults to 1

DIR=""                      # Directory where to make the namelists
	
PARENT_FILE_DIR=""          # Directory for parent region data files,
                            # if left empty, then uses the ORCA2_DATA defined 
                            # in the set-up file 

FORCING_FILE_DIR=""         # Directory for the forcing files for which to create 
                            # on-the-fly interpolation weights 															

SETUP_FILE="msi-hpc.setup"  # Can manually change the setup file. It defaults 
                            # to the setup file used at MSI HPC.

## Parse commandline args with bash built-in getopts  
while getopts ":hs:i:d:n:p:f:r:" opt; do
  case $opt in
    h)
      HELP
      ;;
    s)
			SETUP_FILE = "$OPTARG"
			;;
    d)
			DIR="$OPTARG"
      ;;
    i)
      TEMPLATE_DIR="$OPTARG"
			;;
		n)
			LVL="$OPTARG"
			;;
		p)
			PARENT_FILE_DIR="$OPTARG"
			;;
		f)
			FORCING_FILE_DIR="$OPTARG"
			;;
    r)
      REGION_NAME="$OPTARG"
      ;;
    \?)
      echo -e "Invalid option: -$OPTARG" >&2
      exit 1
      ;;
    :)
      echo -e "Option -$OPTARG requires an argument." >&2
      exit 1
      ;;
  esac
done


## Source the set-up file
if [ -f $SETUP_FILE ]; then
  echo -e "Using ${SETUP_FILE} as the setup file ... "
  source $SETUP_FILE
else 
  echo "Set-up file ${SETUP_FILE} not found!"
  exit 1 
fi

## If empty, set parent files dir to be ORCA2_DATA
if [ ! "${PARENT_FILE_DIR}" ]; then
  PARENT_FILE_DIR=${ORCA2_DATA}
fi

## If empty, set forcing files dir to be ORCA2_DATA
if [ ! "${FORCING_FILE_DIR}" ]; then
  FORCING_FILE_DIR=${ORCA2_DATA}
fi

#3 If empty, set template namelist file dir to be ORCA2_DATA
if [ ! "${TEMPLATE_DIR_NAME}" ]; then
  TEMPLATE_DIR=${NESTED_TEMPLATE_DIR}
fi

## 'Calculate' parent level
if [ ${LVL} -eq 1 ]
then
  PARENT_LVL=""
else
  PARENT_LVL=$((${LVL}-1))_
fi

## Make the output directory
mkdir -p ${DIR}

## Move template files into output directory
cp ${TEMPLATE_DIR}/n_namelist_cfg                         ${DIR}/${LVL}_namelist_cfg   
cp ${TEMPLATE_DIR}/template-chlorophyll-bilinear.weights  ${DIR}/${LVL}_${REGION_NAME}-chlorophyll-bilinear.weights 
cp ${TEMPLATE_DIR}/template-core2-bicubic.weights         ${DIR}/${LVL}_${REGION_NAME}-core2-bicubic.weights 
cp ${TEMPLATE_DIR}/template-core2-bilinear.weights        ${DIR}/${LVL}_${REGION_NAME}-core2-bilinear.weights 
cp ${TEMPLATE_DIR}/template.nesting                       ${DIR}/${LVL}_${REGION_NAME}.nesting


## Replace values in the namelists 
## for file in ${LVL}__namelist_cfg ${LVL}_${REGION_NAME}-chlorophyll-bilinear.weights ${LVL}_${REGION_NAME}-core2-bicubic.weights -core2-bilinear.weights .nesting
for file in ${DIR}/*
do
    ## Insert nesting level
    ${SCRIPTS_DIR}/replace-helper.sh -f ${file} -s ?NESTING-LEVEL? -r ${LVL}

    ## Insert region name 
    ${SCRIPTS_DIR}/replace-helper.sh -f ${file} -s ?REGION-NAME? -r ${REGION_NAME}

    ## Insert path to forcing data dir 
    ${SCRIPTS_DIR}/replace-helper.sh -f ${file} -s ?PATH-TO-FORCING-DIR? -r ${FORCING_FILE_DIR}

    ## Insert path to parent data dir 
    ${SCRIPTS_DIR}/replace-helper.sh -f ${file} -s ?PATH-TO-PARENT-DIR? -r ${PARENT_FILE_DIR}

    ## Insert parent level
    ${SCRIPTS_DIR}/replace-helper.sh -f ${file} -s ?PARENT-LEVEL? -r ${PARENT_LVL}

    ## Insert path to topography data 
    ${SCRIPTS_DIR}/replace-helper.sh -f ${file} -s ?PATH-TO-TOPOGRAPHY-DATA? -r ${TOPOGRAPHY_DATA}
done 

