#!/bin/bash
#
# Author: 	
#		Madis Ollikainen 
#
# Functionality: 	
#		A helper script for downloading or installing netCDF4, HDF5, zlib and szip libraries 
#		for use in the NEMO ocean modelling framework; namely for use in the XIOS I/O system. 
#
# Notes:
#		Large portions of this script have been based on the netCDF installation
#		script provided at https://gist.github.com/perrette/cd815d03830b53e24c82
#
# Further information:
#		netCDF4: http://www.unidata.ucar.edu/software/netcdf/
#		HDF5: https://support.hdfgroup.org/HDF5/
#

## Help Message
SCRIPT=`basename ${BASH_SOURCE[0]}`
function HELP {
	echo -e ""
	echo -e " Helper script for downloads and installs the netCDF4, HDF5, zlib and szip libraries"\\n
  echo -e " Usage: "\\n
  echo -e " Downloading : $SCRIPT -d [lib] (optional: -s [setup])"
  echo -e " Compilation : $SCRIPT -c [lib] (optional: -s [setup])"\\n
  echo -e " Options: "\\n 
  echo -e "  -h\\t\\t: Creates this help message"\\n
  echo -e "  -d [lib]\\t: Use script for downloading. Choose whether to download all or one specific "
  echo -e "             \\t  library. Allowed option are: all, zlib, szip, hdf5, netcdf4, netcdf4-fort."
  echo -e "  -c [lib]\\t: Use script for compiling. Choose which library to compile. "
  echo -e "             \\t  Allowed option are: zlib, szip, hdf5, netcdf4, netcdf4-fort."
  echo -e "  -s [file]\\t: (Optional) Sets [file] as the set-up file. Default 'msi-hpc.setup'"\\n
  echo -e " Notes:"\\n
  echo -e " The script downloads libraries from the Unidata FTP server. The version of the libraries"
  echo -e " are defined in the set-up file. The default values are:"
  echo -e "   szip-v2.1"
  echo -e "   zlib-v1.2.8"
  echo -e "   hdf5-v1.8.13"
  echo -e "   netCDF4-v4.4.1.1"
  echo -e "   netCDF4-Fortran-v4.4.4"
  echo -e ""\\n
  exit 1
}

## Variables to be recieved from commandline options
DOWNLOAD=""									# Either download none, all or one specific
COMPILE=""									# Either compile none, all or one specific
SETUP_FILE="msi-hpc.setup"  # Can manually change the setup file. It defaults 
														# to the setup file used at MSI HPC.
ALLOWED=('all' 'zlib' 'szip' 'hdf5' 'netcdf4' 'netcdf4-fort' 'none')

## Parse commandline args with bash built-in getopts
while getopts ":hs:d:c:" opt; do
  case $opt in
    h)
      HELP
      ;;
    s)
			SETUP_FILE="$OPTARG"
			;;
    d)
			if [[ ${ALLOWED[*]} =~ "$OPTARG" ]] 
			then
				DOWNLOAD=$OPTARG
				echo -e "Downloading $DOWNLOAD"
			else 
      	echo -e "Invalid argument for -d; allowed usage:"\\n
  			echo -e "$SCRIPT -d [none/zlib/szip/hdf5/netcdf4/netcdf4-fort]"\\n
  			exit 1
  		fi
  		;;
		c)
			if [[ ${ALLOWED[*]} =~ "$OPTARG" ]] 
			then
				COMPILE=$OPTARG
				echo -e "Compiling $COMPILE"
			else 
      	echo -e "Invalid argument for -d; allowed usage:"\\n
  			echo -e "$SCRIPT -c [none/zlib/szip/hdf5/netcdf4/netcdf4-fort]"\\n
  			exit 1
  		fi
  		;;
    \?)
      echo -e "Invalid option: -$OPTARG" >&2
      echo -e "Usage: "\\n
  		echo -e "For downloading : $SCRIPT -d [none/zlib/szip/hdf5/netcdf4/netcdf4-fort]"\\n
  		echo -e "For compilation : $SCRIPT -c [none/zlib/szip/hdf5/netcdf4/netcdf4-fort]"\\n
  		echo -e ""\\n
      exit 1
      ;;
    :)
      echo -e "Option -$OPTARG requires an argument." >&2
      echo -e "Allowed options for -c and -d are: none/zlib/szip/hdf5/netcdf4/netcdf4-fort"
      exit 1
      ;;
  esac
done


## Source the set-up file
if [ -f $SETUP_FILE ]; then
	echo -e ""
	echo -e "Using ${SETUP_FILE} as the setup file ... "
	source $SETUP_FILE
else 
	echo "ERROR: Set-up file ${SETUP_FILE} not found!"
	exit 1 
fi

## If necessary, creat general directories
if [ ! -d "${MAIN_DIR}" ]; then
	mkdir -p ${MAIN_DIR}
fi

if [ ! -d "${DOWNLOADS_SERIAL_DIR}" ]; then
	mkdir -p ${DOWNLOADS_SERIAL_DIR}
fi

if [ ! -d "${INSTALL_SERIAL_DIR}" ]; then
	mkdir -p ${INSTALL_SERIAL_DIR}
fi



## Download for Serial HDF5/netCD4 stuff
cd ${DOWNLOADS_SERIAL_DIR}

case $DOWNLOAD in
	none)
		echo -e "Nothing to download ..."		
			;;
	all)
		# szip
		echo ""
		echo "Downloading szip ... "
		echo ""
		wget ftp://ftp.unidata.ucar.edu/pub/netcdf/netcdf-4/szip-${SZIP_VERSION}.tar.gz
		tar -zxvf szip-${SZIP_VERSION}.tar.gz
		echo ""
		# zlib
		echo ""
		echo "Downloading zlib ... "
		echo ""
		wget ftp://ftp.unidata.ucar.edu/pub/netcdf/netcdf-4/zlib-${ZLIB_VERSION}.tar.gz
		tar -zxvf zlib-${ZLIB_VERSION}.tar.gz
		echo ""
		# hdf5
		echo ""
		echo "Downloading hdf5 ... "
		echo ""
		wget ftp://ftp.unidata.ucar.edu/pub/netcdf/netcdf-4/hdf5-${HDF5_VERSION}.tar.gz
		tar -zxvf hdf5-${HDF5_VERSION}.tar.gz
		echo ""	
		# netCDF4-C
		echo ""
		echo "Downloading netCDF4-C ... "
		echo ""
		wget http://www.unidata.ucar.edu/downloads/netcdf/ftp/netcdf-${NETCDF4_C_VERSION}.tar.gz
		tar -zxvf netcdf-${NETCDF4_C_VERSION}.tar.gz
		echo ""

		# ! Correct a buggy h5_test/tst_h_par.c 
		sleep 2
		echo ""
		echo "Correcting bug in h5_test/tst_h_par.c ... "
		echo ""
		echo "Adding '#include \"err_macros.h\"' to tst_h_par.c ... "
		sed '/#include <hdf5.h>/a \#include "err_macros.h"' netcdf-${NETCDF4_C_VERSION}/h5_test/tst_h_par.c > tmp
		mv tmp netcdf-${NETCDF4_C_VERSION}/h5_test/tst_h_par.c
		echo ""
		# netCDF4-Fortran
		echo ""
		echo "Downloading netCDF4-Fortran ... "
		echo ""
		wget http://www.unidata.ucar.edu/downloads/netcdf/ftp/netcdf-fortran-${NETCDF4_FORTRAN_VERSION}.tar.gz
		tar -zxvf netcdf-fortran-${NETCDF4_FORTRAN_VERSION}.tar.gz
		echo ""
		;;
	szip)
		# szip
		echo ""
		echo "Downloading szip ... "
		echo ""
		wget ftp://ftp.unidata.ucar.edu/pub/netcdf/netcdf-4/szip-${SZIP_VERSION}.tar.gz
		tar -zxvf szip-${SZIP_VERSION}.tar.gz
		echo ""
		;;
	zlib)
		# zlib
		echo ""
		echo "Downloading zlib ... "
		echo ""
		wget ftp://ftp.unidata.ucar.edu/pub/netcdf/netcdf-4/zlib-${ZLIB_VERSION}.tar.gz
		tar -zxvf zlib-${ZLIB_VERSION}.tar.gz
		echo ""
		;;
	hdf5)
		# hdf5
		echo ""
		echo "Downloading hdf5 ... "
		echo ""
		wget ftp://ftp.unidata.ucar.edu/pub/netcdf/netcdf-4/hdf5-${HDF5_VERSION}.tar.gz
		tar -zxvf hdf5-${HDF5_VERSION}.tar.gz
		echo ""
		;;
	netcdf4)	
		# netCDF4-C
		echo ""
		echo "Downloading netCDF4-C ... "
		echo ""
		wget http://www.unidata.ucar.edu/downloads/netcdf/ftp/netcdf-${NETCDF4_C_VERSION}.tar.gz
		tar -zxvf netcdf-${NETCDF4_C_VERSION}.tar.gz
		echo ""

		# ! Correct a buggy h5_test/tst_h_par.c 
		sleep 2
		echo ""
		echo "Correcting bug in h5_test/tst_h_par.c ... "
		echo ""
		echo "Adding '#include \"err_macros.h\"' to tst_h_par.c ... "
		sed '/#include <hdf5.h>/a \#include "err_macros.h"' netcdf-${NETCDF4_C_VERSION}/h5_test/tst_h_par.c > tmp
		mv tmp netcdf-${NETCDF4_C_VERSION}/h5_test/tst_h_par.c
		echo ""
		;;
	netcdf4-fort)
		# netCDF4-Fortran
		echo ""
		echo "Downloading netCDF4-Fortran ... "
		echo ""
		wget http://www.unidata.ucar.edu/downloads/netcdf/ftp/netcdf-fortran-${NETCDF4_FORTRAN_VERSION}.tar.gz
		tar -zxvf netcdf-fortran-${NETCDF4_FORTRAN_VERSION}.tar.gz
		echo ""
		;;
esac


## Install Serial HDF5/netCDF4 stuff
cd ${DOWNLOADS_SERIAL_DIR}

case $COMPILE in
	none)
		echo -e "Nothing to compile ..."		
			;;
	szip)
		# Check if szip downloaded
		if [ ! -d "szip-${SZIP_VERSION}" ]; then
			echo -e "Need to download szip before compiling!"
			exit 1
		fi
		# Compile
		cd szip-${SZIP_VERSION}
		CC=${CC_SER} ./configure --prefix=/${INSTALL_SERIAL_DIR}
		make
		make check | tee $SETUP_HOME_DIR/szip-make-check.log  
		make install
			;;
	zlib)	
		# Check if szip downloaded
		if [ ! -d "zlib-${ZLIB_VERSION}" ]; then
			echo -e "Need to download szip before compiling!"
			exit 1
		fi
		# Compile			
		cd zlib-${ZLIB_VERSION}
		CC=${CC_SER} ./configure --prefix=/${INSTALL_SERIAL_DIR}
		make
		make check | tee $SETUP_HOME_DIR/zlib-make-check.log
		make install
			;;
	hdf5)
		# Check if hdf5 downloaded 
		if [ ! -d "hdf5-${HDF5_VERSION}" ]; then
			echo -e "Need to download hdf5 before compiling!"
			exit 1
		fi
		# Check if szip and zlib have been compiled
		# ... TO-DO ...
		# Compile
		cd hdf5-${HDF5_VERSION}
		CC=${CC_SER} ./configure --prefix=/${INSTALL_SERIAL_DIR}/hdf5-${HDF5_VERSION} --with-szlib=${INSTALL_SERIAL_DIR} --with-zlib=${INSTALL_SERIAL_DIR}
		make
		make check | tee $SETUP_HOME_DIR/hdf5-make-check.log
		make install
			;;
	netcdf4)
		# Check if hdf5 downloaded 
		if [ ! -d "netcdf-${NETCDF4_C_VERSION}" ]; then
			echo -e "Need to download netcdf4 before compiling!"
			exit 1
		fi
		# Compile
		# netCDF4-C
		cd netcdf-${NETCDF4_C_VERSION}
		make clean
		# INC_PATH_TEST="-I${INSTALL_SERIAL_DIR}/hdf5-${HDF5_VERSION}/include -I${INSTALL_SERIAL_DIR}/include"
		# echo $INC_PATH_TEST
		# export LD_LIBRARY_PATH=~/NEMO-OCEAN-MODEL/main/${INSTALL_SERIAL_DIR}/lib:~/NEMO-OCEAN-MODEL/main/${INSTALL_SERIAL_DIR}/hdf5-${HDF5_VERSION}/lib:$LD_LIBRARY_PATH
		export LD_LIBRARY_PATH=~/nemo-ocean-model/main/${INSTALL_SERIAL_DIR}/lib:~/nemo-ocean-model/main/${INSTALL_SERIAL_DIR}/hdf5-${HDF5_VERSION}/lib:$LD_LIBRARY_PATH
		CC=${CC_SER}  CPPFLAGS="-I${INSTALL_SERIAL_DIR}/hdf5-${HDF5_VERSION}/include -I${INSTALL_SERIAL_DIR}/include" LDFLAGS="-L${INSTALL_SERIAL_DIR}/hdf5-${HDF5_VERSION}/lib -L${INSTALL_SERIAL_DIR}/lib" ./configure --prefix=${INSTALL_SERIAL_DIR}/netCDF4
		make
		make check | tee $SETUP_HOME_DIR/netcdf4-make-check.log 
		make install
				;;
	netcdf4-fort)
		# Check if hdf5 downloaded 
		if [ ! -d "netcdf-fortran-${NETCDF4_FORTRAN_VERSION}" ]; then
			echo -e "Need to download netcdf4-fortran before compiling!"
			exit 1
		fi
		# netCDF4-Fortran
		cd netcdf-fortran-${NETCDF4_FORTRAN_VERSION}
		export LD_LIBRARY_PATH=${INSTALL_SERIAL_DIR}/hdf5-${HDF5_VERSION}/lib:${INSTALL_SERIAL_DIR}/lib:${INSTALL_SERIAL_DIR}/netCDF4/lib:${LD_LIBRARY_PATH}
		CC=${CC_SER} FC=${FC_SER} F77=${F77_SER} CPPFLAGS="-I${INSTALL_SERIAL_DIR}/hdf5-${HDF5_VERSION}/include -I${INSTALL_SERIAL_DIR}/include -I${INSTALL_SERIAL_DIR}/netCDF4/include" LDFLAGS="-L${INSTALL_SERIAL_DIR}/hdf5-${HDF5_VERSION}/lib -L${INSTALL_SERIAL_DIR}/lib -L${INSTALL_SERIAL_DIR}/netCDF4/lib" ./configure --prefix=${INSTALL_SERIAL_DIR}/netCDF4
		make
		make check | tee $SETUP_HOME_DIR/netcdf4-fort-make-check.log
		make install
			;;
esac

