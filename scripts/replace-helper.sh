#!/bin/bash
#
# Author: 	
#		Madis Ollikainen 
#
# Functionality: 	
#		A helper script for replacing small parts in files.
#		The script is just a wrapper around the 'grep' frunction.

## Help Message
SCRIPT=`basename ${BASH_SOURCE[0]}`
function HELP {
	echo -e ""
	echo -e " A wrapper script around 'sed' for making search-and-replace in files easier."\\n
  echo -e " Usage: ./${SCRIPT} -f [file] -s [key] -r [new value]"\\n
  echo -e " Options: "\\n 
  echo -e "  -h\\t\\t: Creates this help message"\\n
  echo -e "  -f [file]\\t: (Required) Sets the file, where to do the replacement."
  echo -e "  -s [key]\\t: (Required) Sets the keyword, what to look for in the file."
  echo -e "  -r [value]\\t: (Required) Sets the new value, with what to replace all \\n\\t\\t\\t     instances of keyword in the file."
  echo -e ""\\n
  exit 1
} 

## Variables to be recieved from commandline options
FILE=""
SEARCH=""
REPLACE=""

## Parse commandline args with bash built-in getopts 
while getopts ":hf:s:r:" opt; do
  case $opt in
		h)
      HELP
      ;;
    f)
			FILE="$OPTARG"
			;;
		s)
			SEARCH="$OPTARG"
			;;
		r)
			REPLACE="$OPTARG"
			;;
	esac
done

## Do the actual searh-and-replace with grep
sed -i -e 's#'${SEARCH}'#'${REPLACE}'#g' ${FILE}