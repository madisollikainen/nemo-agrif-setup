#!/bin/bash
#
# Author: 	
#		Madis Ollikainen 
#
# Functionality: 	
#		A helper script for downloading and/or installing the XIOS I/O system used in 
#		the NEMO ocean modelling framework. 
#
# Notes:
#		
#		XIOS-1.0 vs. XIOS-2.0:
#
#			There are two version of XIOS: xios-1.0 and xios-2.0; NEMO-v3.6-stable allowes the  		
#			use of either, however in NEMO-v4.0 (expected to be released later in 2017), xios-1.0
#			is being phased out. 
#			
#			Nevertheless, I've currently been using xios-1.0, because the 
#			ORCA2-LIM3-AGRIF configuration, which are shipped with NEMO-v3.6-stable, work 
#			semi-out-of-the-box with xios-1.0; read: I had trouble getting them to work with 
#			xios-2.0 and couldn't find a the issue fast enough. 
#			
#			For long-term use, it would be recommended to shift towards xios-2.0 and 'one-file-mode', 
#			i.e all I/O servers write their output to a single file per output file type. However, 
#			this requires compiling HDF5 in parallel mode, which might also be tricky, depending on 
#			your system. I ran into some error, for which the explanaitions online where: 
#			'sometimes it doesn't work'. This is also a major reason I haven't pushed for xios-2.0 usage;
#			without the added benefit of parallel HDF5/netCDF4 and 'single-file-mode' finding the bugs
#			just didn't seem worth it. 	
#
#		Compilation notes:
#	
#			Compiling xios needs 2(3) architecture based files arch-XXXX.fcm, arch-XXXX.path and optionally
#			arch-XXXX.env. There are a bunch of ready made files for some big cluster in the XIOS/arch directory.
#	 		However, one should always generate a specififc set of files for their own architecture and then 
#			copy those files to the XIOS/arch directory. 
#
# Further information:
#		XIOS: 			http://forge.ipsl.jussieu.fr/ioserver	
#								http://forge.ipsl.jussieu.fr/ioserver/wiki/documentation
#		NEMO I/O : 	http://forge.ipsl.jussieu.fr/nemo/wiki/Users/ModelInterfacing/InputsOutputs#Inputs-OutputsusingXIOS
#		netCDF4: 		http://www.unidata.ucar.edu/software/netcdf/
#		HDF5: 			https://support.hdfgroup.org/HDF5/

# --------------------- #
# --- Help function --- #
# --------------------- #
SCRIPT=`basename ${BASH_SOURCE[0]}`
function HELP {
	echo -e ""\\n
	echo -e "Helper script for downloading and installing the XIOS I/O library"\\n
	echo -e " Usage: "\\n
  echo -e " Downloading : $SCRIPT -d (optional: -s [setup] -v [xios-version])"
  echo -e " Compilation : $SCRIPT -c (optional: -s [setup] -v [xios-version])"\\n
  echo -e " Options: "\\n 
  echo -e "  -h\\t\\t: Creates this help message"\\n
  echo -e "  -d [lib]\\t: Use script for downloading. "
  echo -e "  -c [lib]\\t: Use script for compiling. "
  echo -e "  -s [file]\\t: (Optional) Sets [file] as the set-up file. Default 'msi-hpc.setup'"
  echo -e "  -v [version]\\t: (Optional) Sets which XIOS version to download/compile. Valid" 
  echo -e "              \\t  options are 'xios-1.0' and 'xios-2.0'. Default: 'xios-1.0'"\\n
  echo -e " Notes:"\\n
  echo -e " The script downloads the libraries with the following commands:"\\n
  echo -e "   XIOS-1.0: svn co -r 703 http://forge.ipsl.jussieu.fr/ioserver/svn/XIOS/branchs/xios-1.0"
  echo -e "   XIOS-2.0: svn co http://forge.ipsl.jussieu.fr/ioserver/svn/XIOS/trunk"\\n
  echo -e " When compiling, the script expects there to be XIOS architecture configuration files"
  echo -e " *.path and *.fcm in the arch-info/xios/ directory. The script also expects the existance"
  echo -e " of the scripts/xios-path-file-helper.sh script, which it uses to fill the *.path"
  echo -e " architecture file with correct paths."
  echo -e ""\\n
  exit 1
}

# ------------------------ #
# --- Read cmd options --- #
# ------------------------ #
VERSION="xios-1.0"					# Version of XIOS (currently only xios-1.0 supported)
DOWNLOAD=false							# Flag for specifying whether to download or not
COMPILE=false								# Flag for specifying whether to compile or not
SETUP_FILE="msi-hpc.setup"  # Can manually change the setup file. It defaults 
														# to the setup file used at MSI HPC.

# Parse commandline args with bash built-in getopts 
while getopts ":hs:dcv:" opt; do
  case $opt in
    h)
      HELP
      ;;
    s)
			SETUP_FILE="$OPTARG"
			;;
    v)
			if [[ "$OPTARG" == "xios-1.0" ]] 
			then
				VERSION=$OPTARG
				echo -e "Selected version ${VERSION}"
			elif [[ "$OPTARG" == "xios-2.0" ]]
			then
				VERSION=$OPTARG
				echo -e "Selected version ${VERSION}"
				# echo -e "ERROR: Currently only xios-1.0 is supported!"
  		# 	exit 1
  		else
  			echo -e "Invalid argument for -v; allowed arguments are xios-1.0 and xios-2.0"\\n
  			echo -e "!! Currently only xios-1.0 is implemented !!"\\n
  			exit 1
  		fi
  		;;
		c)
			COMPILE=true
			;;
		d)
			DOWNLOAD=true
			;;
    \?)
      echo -e "Invalid option: -$OPTARG" >&2
      exit 1
      ;;
    :)
      echo -e "Option -$OPTARG requires an argument." >&2
      exit 1
      ;;
  esac
done

# -------------- #
# --- Set-up --- #
# -------------- #

# For setup, just source the setup file 
if [ -f $SETUP_FILE ]; then
	echo -e "Using ${SETUP_FILE} as the setup file ... "
	source $SETUP_FILE
else 
	echo "Set-up file ${SETUP_FILE} not found!"
	exit 1 
fi


# Check if the program_download directory exists
if [ ! -d "${PROG_DOWNLOADS_DIR}" ]; then
	mkdir -p ${PROG_DOWNLOADS_DIR}
fi

# --------------------------------- #
# --- XIOS download and install --- #
# --------------------------------- #

# Download XIOS2 && XIOS1
cd ${PROG_DOWNLOADS_DIR}

case $VERSION in
	xios-1.0)
		# Download
		if [[ "$DOWNLOAD" == "true" ]]
		then
			svn co -r 703 http://forge.ipsl.jussieu.fr/ioserver/svn/XIOS/branchs/xios-1.0 XIOS-1.0
			ln -s ${PROG_DOWNLOADS_DIR}/XIOS-1.0 ${MAIN_DIR}/XIOS-1.0
		fi

		# Compile
		if [[ "$COMPILE" == "true" ]]
		then
			# Check that the xios directory exists
			if [ ! -d "${PROG_DOWNLOADS_DIR}/XIOS-1.0" ]; then
				echo -e "ERROR: Before compiling you need to download XIOS!"
				exit 1
			fi

			# Check for existance of the required arch files
			for f_ext in fcm path
			do
				# Check if file exists
				if [ ! -f "${XIOS_ARCH_DIR}/arch-${XIOS_ARCH_NAME}.${f_ext}" ]; then
				echo -e "ERROR: Missing required architecture file ${XIOS_ARCH_DIR}/arch-${XIOS_ARCH_NAME}.${f_ext}"
				exit 1
				fi
			done 

			# Copy the .fcm arch file 
			cp ${XIOS_ARCH_DIR}/arch-${XIOS_ARCH_NAME}.fcm ${MAIN_DIR}/XIOS-1.0/arch/
			# cp ${XIOS_ARCH_DIR}/arch-${XIOS_ARCH_NAME}.env ${MAIN_DIR}/XIOS-1.0/arch/

			# Use a helper script to generate the correct .path arch file 
			cd ${SETUP_HOME_DIR}
			${SETUP_HOME_DIR}/scripts/xios-path-file-helper.sh -s ${SETUP_HOME_DIR}/${SETUP_FILE} -d ${MAIN_DIR}/XIOS-1.0/
			cd ${PROG_DOWNLOADS_DIR}

			# Use make_xios for compilation
			cd ${MAIN_DIR}/XIOS-1.0 
			# export LD_LIBRARY_PATH=${INSTALL_SERIAL_DIR}/hdf5-${HDF5_VERSION}/lib:${INSTALL_SERIAL_DIR}/lib:${INSTALL_SERIAL_DIR}/netCDF4/lib:${LD_LIBRARY_PATH}
			./make_xios --arch ${XIOS_ARCH_NAME} --netcdf_lib netcdf4_seq
		
		fi
	;;

	xios-2.0)
		# Download
		if [[ "$DOWNLOAD" == "true" ]]
		then
			svn co http://forge.ipsl.jussieu.fr/ioserver/svn/XIOS/trunk XIOS-2.0
			ln -s ${PROG_DOWNLOADS_DIR}/XIOS-2.0 ${MAIN_DIR}/XIOS-2.0
		fi

		# Compile
		if [[ "$COMPILE" == "true" ]]
		then
			# Check that the xios directory exists
			if [ ! -d "${PROG_DOWNLOADS_DIR}/XIOS-2.0" ]; then
				echo -e "ERROR: Before compiling you need to download XIOS!"
				exit 1
			fi

			# Check for existance of the required arch files
			for f_ext in fcm path
			do
				# Check if file exists
				if [ ! -f "${XIOS_ARCH_DIR}/arch-${XIOS_ARCH_NAME}.${f_ext}" ]; then
				echo -e "ERROR: Missing required architecture file ${XIOS_ARCH_DIR}/arch-${XIOS_ARCH_NAME}.${f_ext}"
				exit 1
				fi
			done 

			# Copy the .fcm arch file 
			cp ${XIOS_ARCH_DIR}/arch-${XIOS_ARCH_NAME}.fcm ${MAIN_DIR}/XIOS-2.0/arch/
			# cp ${XIOS_ARCH_DIR}/arch-${XIOS_ARCH_NAME}.env ${MAIN_DIR}/XIOS-2.0/arch/

			# Use a helper script to generate the correct .path arch file 
			cd ${SETUP_HOME_DIR}
			${SETUP_HOME_DIR}/scripts/xios-path-file-helper.sh -s ${SETUP_HOME_DIR}/${SETUP_FILE} -d ${MAIN_DIR}/XIOS-2.0/
			cd ${PROG_DOWNLOADS_DIR}

			# Use make_xios for compilation
			cd ${MAIN_DIR}/XIOS-2.0 
			# export LD_LIBRARY_PATH=${INSTALL_SERIAL_DIR}/hdf5-${HDF5_VERSION}/lib:${INSTALL_SERIAL_DIR}/lib:${INSTALL_SERIAL_DIR}/netCDF4/lib:${LD_LIBRARY_PATH}
			./make_xios --arch ${XIOS_ARCH_NAME} --netcdf_lib netcdf4_seq
		
		fi
	;;
esac
