#!/bin/bash
#
# Author: 	
#		Madis Ollikainen 
#
# Functionality: 	
#		A helper script for reducing the dublicating nature
#		using several 'arch' and 'setup' files

## Help Message
SCRIPT=`basename ${BASH_SOURCE[0]}`
function HELP {
	echo -e ""
	echo -e " A helper script for reducing the dublicating nature of using several 'arch' and 'setup' files."\\n
  echo -e " Usage: ./${SCRIPT} -d [dir] (optional: -s [setup])"\\n
  echo -e " Options: "\\n 
  echo -e "  -h\\t\\t: Creates this help message"\\n
  echo -e "  -d [dir]\\t: (Required) Sets the XIOS directory."
  echo -e "  -s [file]\\t: (Optional) Sets [file] as the set-up file. Default 'msi-hpc.setup'"
  exit 1
}

## Variables to be recieved from commandline options
SETUP_FILE="msi-hpc.setup"  # Can manually change the setup file. It defaults 
														# to the setup file used at MSI HPC.
DIR=""

## Parse commandline args with bash built-in getopts 
while getopts ":hs:d:" opt; do
  case $opt in
  	h)
      HELP
      ;;
    s)
			SETUP_FILE="$OPTARG"
			;;
		d)
			DIR="$OPTARG"
			;;
		\?)
      echo -e "Invalid option: -$OPTARG" >&2
      exit 1
      ;;
    :)
      echo -e "Option -$OPTARG requires an argument." >&2
      exit 1
      ;;
	esac
done

# Source the set-up file
source $SETUP_FILE

# The file name in XIOS arch dir
XIOS_PATH_FILE=${DIR}/arch/arch-${XIOS_ARCH_NAME}.path

# Make an empty .path arch file in xios/arch dir
if [ -f "${XIOS_PATH_FILE}" ]; then
	rm ${XIOS_PATH_FILE}
fi
touch ${XIOS_PATH_FILE}

# Get MPI information from the XIOS path file 
grep 'MPI_INCDIR=' ${XIOS_ARCH_DIR}/arch-${XIOS_ARCH_NAME}.path >> ${XIOS_PATH_FILE}
grep 'MPI_LIBDIR=' ${XIOS_ARCH_DIR}/arch-${XIOS_ARCH_NAME}.path >> ${XIOS_PATH_FILE}
grep 'MPI_LIB=' ${XIOS_ARCH_DIR}/arch-${XIOS_ARCH_NAME}.path >> ${XIOS_PATH_FILE}
echo "" >> ${XIOS_PATH_FILE}

# Get netCDF4 include and lib from SETUP_FILE
echo "NETCDF_INCDIR=\"-I ${INSTALL_SERIAL_DIR}/netCDF4/include\"" >> ${XIOS_PATH_FILE}
echo "NETCDF_LIBDIR=\"-L ${INSTALL_SERIAL_DIR}/netCDF4/lib\"" >> ${XIOS_PATH_FILE}

# Get the netCDF4 compilation flags from XIOS path file 
grep 'NETCDF_LIB=' ${XIOS_ARCH_DIR}/arch-${XIOS_ARCH_NAME}.path >> ${XIOS_PATH_FILE}
echo "" >> ${XIOS_PATH_FILE}

# Get HDF5 include and lib from SETUP_FILE
echo "HDF5_INCDIR=\"-I ${INSTALL_SERIAL_DIR}/hdf5-${HDF5_VERSION}/include -I${INSTALL_SERIAL_DIR}/include\"" >> ${XIOS_PATH_FILE}
echo "HDF5_LIBDIR=\"-L ${INSTALL_SERIAL_DIR}/hdf5-${HDF5_VERSION}/lib -L${INSTALL_SERIAL_DIR}/lib\"" >> ${XIOS_PATH_FILE}

# Get the netCDF4 compilation flags from XIOS path file 
grep 'HDF5_LIB=' ${XIOS_ARCH_DIR}/arch-${XIOS_ARCH_NAME}.path >> ${XIOS_PATH_FILE}
echo "" >> ${XIOS_PATH_FILE}



